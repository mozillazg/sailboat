package behavior

import (
	"errors"
	"net"
)

type Behavior interface {
	Name() string
	ParseOptions(options map[string]interface{}) error
	OnBeforeHandle(protocol string, src, dst net.Conn) (next bool)
	OnBetweenHandle(protocol string, src, dst net.Conn) (next bool)
	OnAfterHandle(protocol string, src, dst net.Conn) (next bool)
}

var behaviors = map[string]Behavior{}
var ErrUnknownBehavior = errors.New("unknown behavior")

func GetBehavior(name string) (Behavior, error) {
	if v, ok := behaviors[name]; ok {
		return v, nil
	}
	return nil, ErrUnknownBehavior
}
