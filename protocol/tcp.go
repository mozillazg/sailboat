package protocol

import (
	"context"
	"io"
	"net"
)

type TCP struct {
}

var _ Protocol = (*TCP)(nil)

func init() {
	protocols["tcp"] = &TCP{}
}

func (t *TCP) Name() string {
	return "TCP"
}

func (t *TCP) ParseOptions(options map[string]interface{}) error {
	return nil
}

func (t *TCP) Handle(ctx context.Context, src, dst net.Conn, onBetweenHandle OnBetweenHandle) error {
	return relay(src, dst)
}

func relay(src, dst net.Conn) error {
	ch := make(chan error, 1)
	go chanCopy(ch, src, dst)
	go chanCopy(ch, dst, src)
	return <-ch
}

func chanCopy(ch chan<- error, src, dst net.Conn) {
	_, err := io.Copy(dst, src)
	// dst.SetReadDeadline(time.Now())
	ch <- err
}
