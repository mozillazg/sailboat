package behavior

import (
	"fmt"
	"net"
	"strconv"
	"time"
)

type Delay struct {
	sleep  time.Duration
	before bool
}

var _ Behavior = (*Delay)(nil)

func init() {
	behaviors["delay"] = &Delay{
		sleep:  time.Second,
		before: true,
	}
}

func (d *Delay) Name() string {
	return "delay"
}

func (d *Delay) ParseOptions(options map[string]interface{}) error {
	if v, ok := options["sleep"]; ok {
		if s, ok := toFloat(v); ok {
			d.sleep = time.Duration(int64(s*1000)) * time.Millisecond
		} else {
			return fmt.Errorf("invalid value of sleep: %s", v)
		}
	}
	if v, ok := options["before"]; ok {
		if b, ok := toBool(v); ok {
			d.before = b
		} else {
			return fmt.Errorf("invalid value of before: %s", v)
		}
	}
	return nil
}

func (d *Delay) OnBeforeHandle(protocol string, src, dst net.Conn) (next bool) {
	if d.before {
		time.Sleep(d.sleep)
	}
	return true
}

func (d *Delay) OnBetweenHandle(protocol string, src, dst net.Conn) (next bool) {
	return true
}

func (d *Delay) OnAfterHandle(protocol string, src, dst net.Conn) (next bool) {
	if !d.before {
		time.Sleep(d.sleep)
	}
	return true
}

func toFloat(n interface{}) (float64, bool) {
	switch n.(type) {
	case int32:
		return float64(n.(int32)), true
	case int64:
		return float64(n.(int64)), true
	case float32:
		return float64(n.(float32)), true
	case float64:
		return float64(n.(float64)), true
	case string:
		if f, err := strconv.ParseFloat(n.(string), 64); err != nil {
			return 0, false
		} else {
			return f, true
		}
	}
	return 0, false
}

func toBool(n interface{}) (value, ok bool) {
	if b, ok := n.(bool); ok {
		return b, true
	}
	if s, ok := n.(string); ok && (s == "true" || s == "false" || s == "1" || s == "0") {
		return s == "true" || s == "1", true
	}
	if f, ok := toFloat(n); ok && (f == 0 || f == 1) {
		return f == 1, true
	}

	return false, false
}
