package protocol

import (
	"context"
	"errors"
	"net"
)

type OnBetweenHandle func(protocol string, src, dst net.Conn) (next bool)

type Protocol interface {
	Name() string
	ParseOptions(options map[string]interface{}) error
	Handle(ctx context.Context, src, dst net.Conn,
		onBetweenHandle OnBetweenHandle) error
}

var protocols = map[string]Protocol{}
var ErrUnknownProtocol = errors.New("unknown protocol")

func GetProtocol(name string) (Protocol, error) {
	if v, ok := protocols[name]; ok {
		return v, nil
	}
	return nil, ErrUnknownProtocol
}
