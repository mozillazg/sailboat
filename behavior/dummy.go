package behavior

import (
	"net"
)

type Dummy struct{}

var _ Behavior = (*Dummy)(nil)

func init() {
	behaviors["dummy"] = &Dummy{}
}

func (d *Dummy) Name() string {
	return "dummy"
}

func (d *Dummy) ParseOptions(options map[string]interface{}) error {
	return nil
}

func (d *Dummy) OnBeforeHandle(protocol string, src, dst net.Conn) (next bool) {
	return true
}

func (d *Dummy) OnBetweenHandle(protocol string, src, dst net.Conn) (next bool) {
	return true
}

func (d *Dummy) OnAfterHandle(protocol string, src, dst net.Conn) (next bool) {
	return true
}
