package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strings"

	"github.com/mozillazg/sailboat"
	"github.com/mozillazg/sailboat/behavior"
	"github.com/mozillazg/sailboat/protocol"
)

const version = "0.1.0"

var gitCommit = "unknown"
var built = "unknown"

type config struct {
	pAddress        string
	bAddress        string
	behaviorName    string
	behaviorOptions map[string]interface{}
	protocolName    string
	protocolOptions map[string]interface{}
}

func main() {
	cfg, err := parseArgs()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	listener, err := net.Listen("tcp", cfg.pAddress)
	if err != nil {
		panic(err)
	}
	log.Printf("listen on %#v", cfg.pAddress)
	log.Printf("	backend: %#v", cfg.bAddress)
	log.Printf("	protocol: %#v", cfg.protocolName)
	log.Printf("	  options: %#v", cfg.protocolOptions)
	log.Printf("	behavior: %#v", cfg.behaviorName)
	log.Printf("	  options: %s", cfg.behaviorOptions)

	p, err := protocol.GetProtocol(cfg.protocolName)
	if err != nil {
		panic(err)
	}
	if err := p.ParseOptions(cfg.protocolOptions); err != nil {
		panic(err)
	}
	b, err := behavior.GetBehavior(cfg.behaviorName)
	if err != nil {
		panic(err)
	}
	if err := b.ParseOptions(cfg.behaviorOptions); err != nil {
		panic(err)
	}
	// sb := sailboat.New("baidu.com:80", p, b)
	sb := sailboat.New(cfg.bAddress, p, b)

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("accept conn failed: %s", conn)
			continue
		}
		log.Printf("accept conn from %s", conn.RemoteAddr())
		go func(conn net.Conn) {
			defer conn.Close()
			if err := sb.HandleConn(context.TODO(), conn); err != nil {
				log.Printf("handle conn %s failed: %s", conn.RemoteAddr(), err)
			}
		}(conn)
	}
}

func parseArgs() (*config, error) {
	pAddress := flag.String("proxy", "127.0.0.1:8080", "address of local proxy")
	bAddress := flag.String("backend", "", "address of backend")
	bp := flag.String("protocol", "tcp",
		`name and config of protocol(<name>[,<k:v[, ...]>]) ("tcp")`)
	bh := flag.String("behavior", "dummy",
		`name and config of behavior(<name>[,<k:v[, ...]>]) ("dummy" or "abort" or "delay")`)
	showVersion := flag.Bool("version", false, "Show version info")
	flag.Parse()

	if *showVersion {
		fmt.Printf(
			`Version:     %s
Git commit:  %s
Built:       %s
`, version, gitCommit, built)
		os.Exit(0)
	}

	if *pAddress == "" {
		return nil, errors.New("invalid value of -proxy")
	}
	if *bAddress == "" {
		return nil, errors.New("invalid value of -backend")
	}
	if *bh == "" {
		return nil, errors.New("invalid value of -behavior")
	}
	cfg := &config{
		pAddress:        *pAddress,
		bAddress:        *bAddress,
		behaviorOptions: map[string]interface{}{},
		protocolOptions: map[string]interface{}{},
	}

	if behaviorName, behaviorOptions, err := parseNestArgs("-behavior", *bh); err != nil {
		return nil, err
	} else {
		cfg.behaviorName = behaviorName
		cfg.behaviorOptions = behaviorOptions
	}
	if protocolName, protocolOptions, err := parseNestArgs("-protocol", *bp); err != nil {
		return nil, err
	} else {
		cfg.protocolName = protocolName
		cfg.protocolOptions = protocolOptions
	}

	return cfg, nil
}

func parseNestArgs(argName, value string) (name string, opts map[string]interface{}, err error) {
	bhArgs := strings.SplitN(value, ",", 2)
	name = bhArgs[0]
	opts = map[string]interface{}{}
	if len(bhArgs) == 2 {
		for _, s := range strings.Split(bhArgs[1], ",") {
			args := strings.SplitN(s, ":", 2)
			if len(args) != 2 {
				return "", nil, fmt.Errorf("invalid value of %s", argName)
			}
			opts[args[0]] = args[1]
		}
	}

	return
}
