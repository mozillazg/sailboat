package behavior

import "net"

type Abort struct {
}

var _ Behavior = (*Abort)(nil)

func init() {
	behaviors["abort"] = &Abort{}
}

func (a *Abort) Name() string {
	return "abort"
}

func (a *Abort) ParseOptions(options map[string]interface{}) error {
	return nil
}

func (a *Abort) OnBeforeHandle(protocol string, src, dst net.Conn) (next bool) {
	src.Close()
	return false
}

func (a *Abort) OnBetweenHandle(protocol string, src, dst net.Conn) (next bool) {
	return true
}

func (a *Abort) OnAfterHandle(protocol string, src, dst net.Conn) (next bool) {
	return true
}
