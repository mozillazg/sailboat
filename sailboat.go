package sailboat

import (
	"context"
	"net"
	"time"

	"github.com/mozillazg/sailboat/behavior"
	"github.com/mozillazg/sailboat/protocol"
)

type Sailboat struct {
	backend     string
	protocol    protocol.Protocol
	behavior    behavior.Behavior
	DialTimeout time.Duration
}

func New(backend string, protocol protocol.Protocol, behavior behavior.Behavior) *Sailboat {
	return &Sailboat{
		backend:     backend,
		protocol:    protocol,
		behavior:    behavior,
		DialTimeout: time.Second * 3,
	}
}

func (s *Sailboat) HandleConn(ctx context.Context, src net.Conn) error {
	backend, err := net.DialTimeout("tcp", s.backend, s.DialTimeout)
	if err != nil {
		return err
	}
	defer backend.Close()

	return s.forward(ctx, src, backend)
}

func (s *Sailboat) forward(ctx context.Context, src, dst net.Conn) error {
	// TODO: handle src closed
	if !s.behavior.OnBeforeHandle(s.protocol.Name(), src, dst) {
		return nil
	}
	err := s.protocol.Handle(ctx, src, dst, s.behavior.OnBeforeHandle)
	s.behavior.OnAfterHandle(s.protocol.Name(), src, dst)
	return err
}
