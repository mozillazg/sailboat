# Sailboat

The Chaos TCP Proxy.

## Usage

```shell
$ sailboat  -backend 127.0.0.1:8002 -proxy 127.0.0.1:8081 -behavior delay,sleep:3
```
